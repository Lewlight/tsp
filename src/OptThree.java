import java.util.ArrayList;

// This solves 3-opt algorithm.
public class OptThree extends OptPath{
	
	public OptThree(Point[] cities, double[][] distanceMatrix) {
		super(cities, distanceMatrix);
	}
	
	public boolean possibleEdgeSwaps() {
		// This method gets all possible edge swaps for 3-opt
		int[] arrayPath = this.arrayPath();
		for(int firstIndex = 0; firstIndex < cities.length; firstIndex++) {
			int firstEdge = arrayPath[firstIndex];
			for(int secondIndex = firstIndex + 1; secondIndex < cities.length; secondIndex++) {
				int secondEdge = arrayPath[secondIndex];
				for(int thirdIndex = secondIndex + 1; thirdIndex < cities.length; thirdIndex++) {
					// we do checks for valid edges to be checked.
					int thirdEdge = arrayPath[thirdIndex];
					int cityOneGoal = getCityGoal(firstEdge);
					int cityTwoGoal = getCityGoal(secondEdge);
					int cityThreeGoal = getCityGoal(thirdEdge);
					if(cityOneGoal == secondEdge || cityOneGoal == thirdEdge  || cityTwoGoal == firstEdge || cityTwoGoal == thirdEdge || cityThreeGoal == firstEdge || cityThreeGoal == secondEdge) continue;
					int[] edges = {firstEdge, secondEdge, thirdEdge};
					// we check if the edges swapped lead to a better path
					PathSwaps swaps = checkEdges(edges);
					if (!swaps.listEdges.isEmpty()) {
						// we make the swap
						this.swapEdges(swaps);
						return true;
					}
				}
			}
		}
		return false;
	}
	
	public PathSwaps checkEdges(int[] edges) {
		// this method looks for a good edge swap that leads to shorter path.
		ArrayList<int[]> listEdges = new ArrayList<int[]>();
		int firstGoal = getCityGoal(edges[0]);
		int secondGoal = getCityGoal(edges[1]);
		int thirdGoal = getCityGoal(edges[2]);
		double firstDistance = getCityEdge(edges[0]) + getCityEdge(edges[1]) + getCityEdge(edges[2]);
		// firstDistance is the original distance of the current edges in the path
		// there are 7 possible swaps using these 3 edges, we get all of their distances.
		double secondDistance = getDistance(edges[0], edges[1]) + getDistance(firstGoal, secondGoal) + getCityEdge(edges[2]);
		double thirdDistance = getDistance(edges[1], edges[2]) + getDistance(thirdGoal, secondGoal) + getCityEdge(edges[0]);
		double fourthDistance = getDistance(edges[0], edges[2]) + getDistance(firstGoal, thirdGoal) + getCityEdge(edges[1]);
		double fifthDistance = getDistance(edges[0], edges[1]) + getDistance(firstGoal, edges[2]) + getDistance(secondGoal, thirdGoal);
		double sixthDistance = getDistance(edges[0], edges[2]) + getDistance(firstGoal, secondGoal) + getDistance(edges[1], thirdGoal);
		double seventhDistance = getDistance(edges[0], secondGoal) + getDistance(edges[2], edges[1]) + getDistance(firstGoal, thirdGoal);
		double eighthDistance = getDistance(edges[0], secondGoal) + getDistance(edges[2], firstGoal) + getDistance(edges[1], thirdGoal);
		double[] distances = {firstDistance, secondDistance, thirdDistance, fourthDistance, fifthDistance, sixthDistance, seventhDistance, eighthDistance};
		// we find the minimum distance of all of them and we use the edge swap resulting in it.
		// leading to the shortest path possible using the 3 edges passed here.
		int minimum = findMin(distances);
		// for each case of these distances there are different ways to  rearrange the cities in the path.
		// The first three cases are basically 2-opt changes
		// The other cases represent the remaining 4 possible 3 edge swaps.
		if(minimum == 1) {
			return twoOptSwaps(edges[0], edges[1], firstDistance, secondDistance);
		}
		else if(minimum == 2) {
			return twoOptSwaps(edges[1], edges[2], firstDistance, thirdDistance);
		}
		else if(minimum == 3) {
			return twoOptSwaps(edges[0], edges[2], firstDistance, fourthDistance);
		}
		else if(minimum == 4) {
			int[] firstSwap = {edges[0], edges[1]};
			listEdges.add(firstSwap);
			int[] secondSwap = {firstGoal, edges[2]};
			listEdges.add(secondSwap);
			int[] thirdSwap = {secondGoal, thirdGoal};
			listEdges.add(thirdSwap);
			ArrayList<Integer> reversedPath = new ArrayList<Integer>();
			reversedPath.add(firstGoal);
			int goalsGoal = firstGoal;
			do {
				goalsGoal = getCityGoal(goalsGoal);
				reversedPath.add(goalsGoal);
			}while(goalsGoal != edges[1]);
			for(int cityIndex = reversedPath.size()-1; cityIndex > 0; cityIndex--) {
				int[] partReversed = {reversedPath.get(cityIndex), reversedPath.get(cityIndex - 1)};
				listEdges.add(partReversed);
			}
			reversedPath = new ArrayList<Integer>();
			reversedPath.add(secondGoal);
			goalsGoal = secondGoal;
			do {
				goalsGoal = getCityGoal(goalsGoal);
				reversedPath.add(goalsGoal);
			}while(goalsGoal != edges[2]);
			for(int cityIndex = reversedPath.size()-1; cityIndex > 0; cityIndex--) {
				int[] partReversed = {reversedPath.get(cityIndex), reversedPath.get(cityIndex - 1)};
				listEdges.add(partReversed);
			}
			return new PathSwaps(listEdges, firstDistance, fifthDistance);
		}
		else if(minimum == 5) {
			int[] firstSwap = {edges[0], edges[2]};
			listEdges.add(firstSwap);
			int[] secondSwap = {secondGoal, firstGoal};
			listEdges.add(secondSwap);
			int[] thirdSwap = {edges[1], thirdGoal};
			listEdges.add(thirdSwap);
			ArrayList<Integer> reversedPath = new ArrayList<Integer>();
			reversedPath.add(secondGoal);
			int goalsGoal = secondGoal;
			do {
				goalsGoal = getCityGoal(goalsGoal);
				reversedPath.add(goalsGoal);
			}while(goalsGoal != edges[2]);
			for(int cityIndex = reversedPath.size()-1; cityIndex > 0; cityIndex--) {
				int[] partReversed = {reversedPath.get(cityIndex), reversedPath.get(cityIndex - 1)};
				listEdges.add(partReversed);
			}
			return new PathSwaps(listEdges, firstDistance, sixthDistance);
		}
		else if(minimum == 6) {
			int[] firstSwap = {edges[0], secondGoal};
			listEdges.add(firstSwap);
			int[] secondSwap = {edges[2], edges[1]};
			listEdges.add(secondSwap);
			int[] thirdSwap = {firstGoal, thirdGoal};
			listEdges.add(thirdSwap);
			ArrayList<Integer> reversedPath = new ArrayList<Integer>();
			reversedPath.add(firstGoal);
			int goalsGoal = firstGoal;
			do {
				goalsGoal = getCityGoal(goalsGoal);
				reversedPath.add(goalsGoal);
			}while(goalsGoal != edges[1]);
			for(int cityIndex = reversedPath.size()-1; cityIndex > 0; cityIndex--) {
				int[] partReversed = {reversedPath.get(cityIndex), reversedPath.get(cityIndex - 1)};
				listEdges.add(partReversed);
			}
			return new PathSwaps(listEdges, firstDistance, seventhDistance);
		}
		else if(minimum == 7) {
			int[] firstSwap = {edges[0], secondGoal};
			listEdges.add(firstSwap);
			int[] secondSwap = {edges[2], firstGoal};
			listEdges.add(secondSwap);
			int[] thirdSwap = {edges[1], thirdGoal};
			listEdges.add(thirdSwap);
			return new PathSwaps(listEdges, firstDistance, eighthDistance);
		}
		return new PathSwaps(listEdges, 0, 0);
	}
	
	// This method finds the index corresponding to the lowest value in the array.
	public int findMin(double[] distances) {
		double minValue = distances[0];
		int index = 0;
		for(int i =0; i < distances.length; i++) {
			if(minValue > distances[i]) {
				minValue = distances[i];
				index = i;
			}
		}
		return index;
	}
	
	// This method finds 2-opt swaps giving 2 edges.
	public PathSwaps twoOptSwaps(int firstEdge, int secondEdge, double oldDistance, double newDistance) {
		ArrayList<int[]> listEdges = new ArrayList<int[]>();
		int firstGoal = getCityGoal(firstEdge);
		int secondGoal = getCityGoal(secondEdge);
		int[] firstSwap = {firstEdge, secondEdge};
		listEdges.add(firstSwap);
		int[] secondSwap = {firstGoal, secondGoal};
		listEdges.add(secondSwap);
		ArrayList<Integer> reversedPath = new ArrayList<Integer>();
		reversedPath.add(firstGoal);
		int goalsGoal = firstGoal;
		do {
			goalsGoal = getCityGoal(goalsGoal);
			reversedPath.add(goalsGoal);
		} while(goalsGoal != secondEdge);
		for(int cityIndex = reversedPath.size()-1; cityIndex > 0; cityIndex--) {
			int[] partReversed = {reversedPath.get(cityIndex), reversedPath.get(cityIndex - 1)};
			listEdges.add(partReversed);
		}
		return new PathSwaps(listEdges, oldDistance, newDistance);
	}
}
