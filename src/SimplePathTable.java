
// A simple class for mapping of integers (city names) another integer
// representing a path where each city is connected to another.
// This assumes that the city names are: 1, 2, 3... n
public class SimplePathTable {
	public int[] table;
	
	public SimplePathTable(int size) {
		table = new int[size];
	}
	
	// we remove 1 from the key inputed because each city corresponds to index
	// before it (e.g. city name 1 will be saved in index 0).
	public int get(int key) {
		return table[key-1];
	}
	
	public void put(int key, int value) {
		table[key-1] = value;
	}
}
