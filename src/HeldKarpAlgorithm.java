import java.util.ArrayList;

// This class is used to solver TSP using Held-Karp Algorithm.
public class HeldKarpAlgorithm {
	final String arrow = "->";
	private final Point[] cities;
	private final double[][] distanceMatrix;
	
	public HeldKarpAlgorithm(Point[] cities, double[][] distanceMatrix){
		this.cities = cities;
		this.distanceMatrix = distanceMatrix;
	}
	
	//This method finds the optimal path for the TSP problem using Held-Karp
	public double startSearch() {
		int numOfCities = cities.length;
		//tables used for Held-Karp algorithm.
		//The costTable saves the lowest cost to go through a set of cities
		Table<Double> costTable = new Table<Double>();
		//The pathTable saves the actual path that has the lowest cost for a set of cities.
		Table<String> pathTable = new Table<String>();
		int[] cityNames = new int[numOfCities - 1];
		//we initialise the tables with data to launch the loop.
		for(int city = 2; city <= numOfCities; city++) {
			int[] key = {city};
			//The key we will use is the array of integers cities.
			//It is also important to note that the meaning of the array is basically
			//The first value is the city that the set of cities leads to
			//The rest of the values in the array is the set of cities (path)
			costTable.put(key, calculateDistance(city, 1));
			pathTable.put(key, "1");
			cityNames[city - 2] = city;
		}
		//we now find the lowest cost for going through each set of cities
		//the length of sets starts from 1 and goes up until number of cities -1
		for(int setLength = 1; setLength < numOfCities; setLength++) {
			int[] empty = new int[0];
			ArrayList<int[]> permutations = new ArrayList<int[]>();
			//we get all permutations for the length of cities that we specify
			//which are keys that will be saved into our tables. (all possible sets)
			//with all possible goal cities for that set.
			getPermutations(permutations, cityNames, setLength - 1, 0, empty);
			for(int[] path: permutations) {
				//we save the cost of each set
				saveCost(path, costTable, pathTable);
			}
		}
		int[] allCities = new int[numOfCities];
		//we add the return path to return to the beginning.
		for(int i =1; i <= numOfCities; i++) {
			allCities[i - 1] = i;
		}
		saveCost(allCities, costTable, pathTable);
		// We print the cost and the path.
		System.out.println(costTable.get(allCities));
		System.out.println(1 + arrow + pathTable.get(allCities));
		return costTable.get(allCities);
	}
	
	public void saveCost(int[] path, Table<Double> costTable, Table<String> pathTable) {
		// This method takes a path and saves the it's cost and the path in the table.
		// we start by getting the goal city.
		int goalCity = path[0];
		// We will get all the paths that lead to the goal city that were saved
		// already in the cost table
		double[] costs = new double[path.length - 1];
		String[] pathHeads = new String[path.length - 1];
		// We use every other city as the goal city of the sub-path
		// which we will then compare to find the shortest path.
		for(int subPathGoalIndex = 1; subPathGoalIndex < path.length; subPathGoalIndex++) {
			int expandedPathHead = path[subPathGoalIndex];
			int[] expandedPath = new int[path.length - 1];
			// We then create that sub-path that leads to the goal city
			// to be used for comparison
			int index = 1;
			for(int otherCityIndex = 1; otherCityIndex < path.length; otherCityIndex++) {
				if(otherCityIndex == subPathGoalIndex) continue;
				expandedPath[index++] = path[otherCityIndex];
			}
			expandedPath[0] = expandedPathHead;
			// we save the paths, and their costs to find the best later.
			pathHeads[subPathGoalIndex-1] = expandedPathHead + arrow + pathTable.get(expandedPath);
			costs[subPathGoalIndex-1] = calculateDistance(goalCity, expandedPathHead) + costTable.get(expandedPath);
		}
		// we find the path with the minimum cost (smallest distance)
		double minCost = costs[0];
		int minCostIndex = 0;
		for(int i = 0; i < costs.length; i++) {
			double cost = costs[i];
			if(cost < minCost) {
				minCost = cost;
				minCostIndex = i;
			}
		}
		// we save that smallest path, and its cost.
		pathTable.put(path, pathHeads[minCostIndex]);
		costTable.put(path, minCost);
	}
	
	public void getPermutations(ArrayList<int[]> result, int[] cities, int length, int start, int[] initial) {
		// This method gets all permutations of cities of a given size
		// it fills up the list given as argument with all of those permutations.
		// This method uses a form of recursion to get all the permutations.
		for(int cityIndex = start; cityIndex < cities.length; cityIndex++) {
			// We copy the list already inputed.
			int[] newInitial = new int[initial.length + 1];
			for(int copyIndex = 0; copyIndex < initial.length; copyIndex++) {
				newInitial[copyIndex] = initial[copyIndex];
			}
			// we add a city to the array of cities. slowly growing the size
			newInitial[initial.length] = cities[cityIndex];
			if(newInitial.length == length) {
				// Once we reach the length we needed we add any city that doesn't
				// already exist in the array as the goal city of the path made from the other
				// cities, in the array.
				for(int goalCity: cities) {
					if (checkIfExists(newInitial, goalCity)) continue;
					int[] addedGoal = new int[newInitial.length + 1];
					// We shift the cities to the right
					for(int index = 0; index < newInitial.length; index++) {
						addedGoal[index + 1] = newInitial[index];
					}
					// We add the goal to the beginning of the array.
					addedGoal[0] = goalCity;
					// We add the array to the list.
					result.add(addedGoal);
				}
			}
			// If we didn't reach the length by adding one city we call it again.
			else {
				getPermutations(result, cities, length, cityIndex+1, newInitial);
			}
		}
	}
	
	public boolean checkIfExists(int[] arr, int val) {
		// This method takes an array of integers and checks if an integers value exists in it.
		// simply iterate until found if not return false.
		for(int in: arr) {
			if(val == in)  return true;
		}
		return false;
	}
	
	// This method gets the distance of 2 cities from the distance matrix.
	public double calculateDistance(int city1, int city2){
		return distanceMatrix[city1 - 1][city2 - 1];
	}
}
