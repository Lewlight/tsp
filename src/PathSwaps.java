import java.util.ArrayList;

// This stores edge swaps for a path, and the old distance of the old edges,
// and the new distance of the new edges.
public class PathSwaps {
	public ArrayList<int[]> listEdges;
	public double oldDistance, newDistance;	


	public PathSwaps(ArrayList<int[]> listEdges, double oldDistance, double newDistance) {
		this.listEdges = listEdges;
		this.oldDistance = oldDistance;
		this.newDistance = newDistance;
	}
}
