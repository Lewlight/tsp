import java.util.ArrayList;

// This class solves TSP using a faster implementation of 2-opt a method found by Kenneth Steiglitz, and Peter Weiner.
public class FastOptTwo extends OptTwo{
	
	public SimpleClosestCityTable closestNodes;
	
	// The scope argument can vary to adjust the performance of the algorithm
	// generally bigger scope means shorter path, but slower execution time.
	public FastOptTwo(Point[] cities, double[][] distanceMatrix, int scope) {
		super(cities, distanceMatrix);
		closestNodes = new SimpleClosestCityTable(cities.length);
		scope = Math.min(scope, cities.length);
		for(int cityNum = 0; cityNum < cities.length; cityNum++) {
			// we find the closest cities we keep the number of these cities to be equal to scope.
			// before we start the algorithm.
			double[] distances = distanceMatrix[cityNum];
			ArrayList<Integer> alreadyAdded = new ArrayList<Integer>();
			for(int iteration =0; iteration < scope; iteration++) {
				double minimumDist = Double.MAX_VALUE;
				int cityIndex = -1;
				for(int otherCityIndex  = 0; otherCityIndex < distances.length; otherCityIndex++) {
					double dist = distances[otherCityIndex];
					if (alreadyAdded.contains(otherCityIndex + 1) || otherCityIndex == cityNum) continue;
					if (minimumDist > dist) {
						minimumDist = distances[otherCityIndex];
						cityIndex = otherCityIndex;
					}
				}
				alreadyAdded.add(cityIndex + 1);
			}
			closestNodes.put(cityNum + 1, alreadyAdded);
		}
	}
	
	public boolean possibleEdgeSwaps(){
		// we find the possible edge swaps.
		for(int cityIndex = 1; cityIndex <= cities.length; cityIndex++) {
			ArrayList<Integer> closestCities = closestNodes.get(cityIndex);
			for(int otherCityIndex = 0; otherCityIndex < closestCities.size(); otherCityIndex++) {
				int closeCity = closestCities.get(otherCityIndex);
				// if we reach a city that is already in the edge then we don't need
				// to check further down the list as any other city will not reduce the length.
				if(closeCity == getCityGoal(cityIndex)) break;
				int[] edgeSwap = {cityIndex, closeCity};
				// we check if the edges swapped lead to a better path.
				PathSwaps swaps = checkEdges(edgeSwap);
				if (!swaps.listEdges.isEmpty()) {
					// we make the swap if it's good.
					this.swapEdges(swaps);
					return true;
				}
			}
		}
		return false;		
	}
}
