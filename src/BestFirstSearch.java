import java.util.ArrayList;

// This class is used to solve TSP using best first search.
public class BestFirstSearch {
	final String arrow = "->";
	private int[] path;
	private double cost;
	private Point[] cities;
	private double[][] distanceMatrix;
	
	public BestFirstSearch(Point[] cities, double[][] distanceMatrix) {
		this.cities = cities;
		this.distanceMatrix = distanceMatrix;
	}
	
	public double startSearch() {
		//This method solves using a best first search approach.
		// This array will store the path.
		path = new int[cities.length];
		// we make the start to be city1.
		path[0] = 1;
		SimpleClosestCityTable closestNodes = new SimpleClosestCityTable(cities.length);
		cost = 0;
		int scope = cities.length - 1;
		ArrayList<Integer> alreadyPathed = new ArrayList<Integer>();
		alreadyPathed.add(1);
		// This loop populates the table saving for each city the closest cities to it
		// in  a list.
		for(int cityNum = 0; cityNum < cities.length; cityNum++) {
			// We get the distances to a certain point and we order them and
			// save them to the table.
			double[] distances = distanceMatrix[cityNum];
			ArrayList<Integer> alreadyAdded = new ArrayList<Integer>();
			for(int iterNum =0; iterNum < scope; iterNum++) {
				double minimumDist = Double.MAX_VALUE;
				int cityIndex = -1;
				// We sort the cities closest to each city (excluding the same city)
				for(int city  = 0; city < distances.length; city++) {
					double dist = distances[city];
					// If we have already added the city or if we are checking the same city
					// we skip this iteration.
					if (alreadyAdded.contains(city + 1) || city == cityNum) continue;
					if (minimumDist > dist) {
						minimumDist = distances[city];
						cityIndex = city;
					}
				}
				alreadyAdded.add(cityIndex + 1);
			}
			closestNodes.put(cityNum + 1, alreadyAdded);
		}
		// We no start the actual loop that builds the path.
		for(int cityIndex = 1; cityIndex < path.length; cityIndex++) {
			// We get the previous city
			int lastNode = path[cityIndex-1];
			ArrayList<Integer> closest = closestNodes.get(lastNode);
			// We find the closest city.
			for(int city: closest) {
				// We ignore the cities already added the path.
				if(alreadyPathed.contains(city)) {
					continue;
				}
				cost += distanceMatrix[lastNode - 1][city - 1];
				path[cityIndex] = city;
				alreadyPathed.add(city);
				break;
			}
		}
		// We add to the cost the return from the last city in the path to city 1.
		cost+= distanceMatrix[path[path.length-1] - 1][0];
		return cost;
	}
	
	public void printPathFound() {
		// This method prints the path found.
		System.out.println();
		for(int cityName: path) {
			System.out.print(arrow + cityName);
		}
		// we add the city 1 to the end.
		System.out.print(arrow + 1);
		
		System.out.println("The cost is: " + cost);
	}
}
