import java.util.ArrayList;

//This class represents a Path (used only to solve Dijkstra)
public class Path {
	final String arrow = "->";
	public Point[] path;
	public ArrayList<Integer> missing;
	public double pathLength;
	
	//This constructor takes an old path and a city, and it creates a path with
	// with the new city added to the end of the path.
	public Path(Path oldPath, Point newPoint) {
		this.missing = new ArrayList<Integer>(oldPath.missing);
		path = new Point[oldPath.size() + 1];
		// we copy the cities to a bigger path.
		for(int cityIndex = 0; cityIndex < oldPath.size(); cityIndex++) {
			Point cityToAdd = oldPath.path[cityIndex];
			this.path[cityIndex] = cityToAdd;
		}
		// we add the new city to the end of the path.
		this.path[oldPath.size()] = newPoint;
		missing.remove(Integer.valueOf(newPoint.getName()));
		pathLength = oldPath.pathLength + this.path[this.size() - 1].distanceToPoint(this.path[this.size() -2]);
	}
	
	// if we call it with one point we make a path with only one Point.
	public Path(Point onePoint, int maxSize) {
		path = new Point[1];
		pathLength = 0;
		path[0] = onePoint;
		missing = new ArrayList<Integer>();
		for(int missingCityIndex = 1; missingCityIndex <= maxSize; missingCityIndex++) {
			if(missingCityIndex == onePoint.getName()) {
				continue;
			}
			missing.add(missingCityIndex);
		}
	}
	
	// This returns the size of the path.
	public int size() {
		return this.path.length;
	}
	
	// This prints the path.
	public void print() {
		System.out.println("printing path");
		for(Point city: path) {
			System.out.print(city.getName() + arrow);
		}
		System.out.println();
		System.out.println("Size is " + pathLength);
	}
	

}
