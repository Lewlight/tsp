
// This is a hash table with keys being arrays of integers
// and values are nodes (linked lists) with generic values.
public class Table<V> {
	private Node<V>[] array;
	private int[] primaryNumbers;
	
	public Table() {
		array = new Node[10000];
		int[] numbers = {3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37};
		primaryNumbers = numbers;
	}
	
	// This returns the value corresponding to the key.
	public V get(int[] key){
		// we get the index.
		int index = Math.abs(hashKey(key)%array.length);
		Node<V> linkedList = array[index];
		// and we search for the node with equal keys
		do {
			if(compareKeys(linkedList.key, key)) {
				return linkedList.value;
			}
			linkedList = linkedList.next;
		}while(linkedList != null);
		return null;
	}
	
	// This puts a value in a specific key location.
	public void put(int[] key, V value){
		// we get the index.
		int index = Math.abs(hashKey(key)%array.length);
		Node<V> linkedList = array[index];
		// if at the index we find null, then we place the new node there.
		if (linkedList == null) {
			array[index] = new Node<V>(key, value);
		}
		// if at the index there is a node and the node has equal key, then we replace the node with
		// the new one with the new value.
		else if(compareKeys(linkedList.key, key)) {
			array[index] = new Node<V>(key, value);
		}
		// otherwise we loop through the linked list, until we reached the end or if we
		// find a node with an equal key
		else {
			boolean placedTheNode = false;
			while(!placedTheNode) {
				// if we find null we reached the end we just place the node there
				if(linkedList.next == null) {
					linkedList.next = new Node<V>(key, value);
					placedTheNode = true;
				}
				if(compareKeys(linkedList.key, key)) {
					// if we found a node with the same key, we replace it there.
					linkedList.next = new Node<V>(key, value, linkedList.next.next);
					placedTheNode = true;
				}
				else {
					linkedList = linkedList.next;
				}				
				
			}
		}
		
	}
	
	private int hashKey(int[] key) {
		// This calculates a hash value for the integer array used as key in our table
		int code = 0;
		for(int index = 0; index < key.length; index++) {
			// we multiply each element by a primary number from the array.
			code += key[index] * (primaryNumbers[index%primaryNumbers.length]) + index;
		}
		return code;		
	}
	
	// This compares two integer arrays to check that they are equal in values.
	private boolean compareKeys(int[] key, int[] otherKey) {
		if(key.length != otherKey.length) {
			return false;
		}
		for(int index = 0; index < key.length; index++) {
			if(key[index] != otherKey[index]) {
				return false;
			}
		}
		return true;
	}
}
