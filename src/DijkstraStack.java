
// This class represents the stack used by my implementation of Dijkstra.
public class DijkstraStack {
	private Path[] stack;
	private Point[] cities;
	private Path bestPath;
	
	public DijkstraStack(Point[] cities) {
		// The initial stack has only one path (the start city 1).
		stack = new Path[1];
		stack[0] = new Path(cities[0], cities.length);
		this.cities = cities;
	}
	
	public void expandHead() {
		// This method takes the head of the stack and expands it,
		// meaning it adds any city that doesn't already exist in the path to it.
		Path headPath = stack[stack.length - 1];
		Path[] temporaryStack = new Path[stack.length - 1 + headPath.missing.size()];
		// Create a copy of the path.
		for(int cityIndex = 0; cityIndex < stack.length - 1; cityIndex++) {
			temporaryStack[cityIndex] = stack[cityIndex];
		}
		for(int missingCityIndex = 0; missingCityIndex < headPath.missing.size(); missingCityIndex++) {
			// We then create a new Path with each missing city (city that doesn't already exist in the path).
			// We then add it to the stack while keeping the stack sorted.
			int cityName = headPath.missing.get(missingCityIndex);
			Point city = cities[cityName - 1];
			// We get the missing city and create an expanded path for each city.
			Path expandedPath = new Path(headPath, city);
			// We then insert these cities to the temporaryPath where we will add the new
			// expanded Paths, to the stack in a sorted way.
			for(int cityIndex = 0; cityIndex < temporaryStack.length; cityIndex++) {
				Path compared = temporaryStack[cityIndex];
				if (compared == null){
					temporaryStack[cityIndex] = expandedPath;
					break;
				}
				// We shift the stack if the path needs to be added to the middle
				else if(compared.pathLength < expandedPath.pathLength) {
					for(int indexToShift = temporaryStack.length-1; indexToShift > cityIndex; indexToShift--) {
						temporaryStack[indexToShift] = temporaryStack[indexToShift-1];
					}
					temporaryStack[cityIndex] = expandedPath;
					break;
				}
			}	
		}
		// Replace the stack with new stack.
		stack = temporaryStack;
	}
	
	// This method solves Dijkstra algorithm
	public double startSearch(){
		// We get the head of a stack (shortest path in queue) to expand.
		boolean foundShortestComplete = false;
		while(!foundShortestComplete){
			Path headPath = stack[stack.length - 1];
			if (headPath.size() == cities.length+1) {
				// if the size of a headPath reaches the length of cities + 1 it means it
				// completed a loop through all cities and back to 1.
				bestPath = headPath;
				foundShortestComplete = true;
			}
			else if (headPath.size() == cities.length) {
				// if the size of a headPath reaches the length of cities, this means 
				// it has gone through all cities, and it needs to go back to city 1 now.
				// the way we do it is by adding city 1 to the list of missing cities.
				headPath.missing.add(1);
			}
			if(!foundShortestComplete){
				expandHead();
			}
		}
		bestPath.print();
		return bestPath.pathLength;
	}
	
	public void printPathFound(){
		// This method prints the path found.
		if (bestPath == null){
			System.out.println("This object hasn't called start search method yet, run the algorithm first.");
		}
		else{
			bestPath.print();
		}
	}
}
