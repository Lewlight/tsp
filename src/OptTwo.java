import java.util.ArrayList;

// This class solves TSP using 2-opt algorithm.
public class OptTwo extends OptPath{
	
	public OptTwo(Point[] cities, double[][] distanceMatrix) {
		super(cities, distanceMatrix);
	}
	
	//This method finds valid edges swaps for 2-opt
	@Override
	public boolean possibleEdgeSwaps() {
		for(int cityName = 1; cityName < cities.length; cityName++) {
			for(int otherCityName = cityName + 1; otherCityName <= cities.length; otherCityName++) {
				int cityOneGoal = getCityGoal(cityName);
				int cityTwoGoal = getCityGoal(otherCityName);
				// This condition prevents the checks of invalid swaps
				// these invalid swaps when done lead to paths becoming invalid (e.g. start city is no longer start)
				if(cityOneGoal == otherCityName || cityTwoGoal == cityName) continue;
				// we represent an edge by one city since each city in the path is connected to one other
				// city so a city and the city it goes to is the edge and we represent it by the
				// city it starts with.
				int[] edges = {cityName, otherCityName};
				// we check if the edges swapped lead to a better path
				PathSwaps swaps = checkEdges(edges);
				if (!swaps.listEdges.isEmpty()) {
					// we make the swap
					this.swapEdges(swaps);
					return true;
				}
			}
		}
		return false;
	}
	
	public PathSwaps checkEdges(int[] edges) {
		ArrayList<int[]> listEdges = new ArrayList<int[]>();
		int firstGoal = getCityGoal(edges[0]);
		int secondGoal = getCityGoal(edges[1]);
		// first distance is the distance of the unchanged edges
		double firstDistance = getCityEdge(edges[0]) + getCityEdge(edges[1]);
		// second distance is the distance of the new edges.
		double secondDistance = getDistance(edges[0], edges[1]);
		secondDistance += getDistance(firstGoal, secondGoal);
		
		if (secondDistance < firstDistance) {
			// if the new edges are better we build the new path
			// by having a list of arrays, of size 2, the arrays first element
			// is a city and the second element is the new city it points to.
			// we first add the two swapped edges
			int[] firstSwap = {edges[0], edges[1]};
			listEdges.add(firstSwap);
			int[] secondSwap = {firstGoal, secondGoal};
			listEdges.add(secondSwap);
			// we then need to reverse part of the path to keep the path valid.
			ArrayList<Integer> reversedPath = new ArrayList<Integer>();
			reversedPath.add(firstGoal);
			int goalsGoal = firstGoal;
			do {
				goalsGoal = getCityGoal(goalsGoal);
				reversedPath.add(goalsGoal);
			}while(goalsGoal != edges[1]);
			for(int cityIndex = reversedPath.size()-1; cityIndex > 0; cityIndex--) {
				int[] partReversed = {reversedPath.get(cityIndex), reversedPath.get(cityIndex - 1)};
				listEdges.add(partReversed);
			}
		}
		PathSwaps result = new PathSwaps(listEdges, firstDistance, secondDistance);
		return result;
	}
}
