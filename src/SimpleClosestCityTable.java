import java.util.ArrayList;

// A simple class for mapping of integers (city names) to Lists
// where the city name - 1 is the index corresponding to the list for that city
// This assumes that the city names are: 1, 2, 3... n
public class SimpleClosestCityTable {
	public ArrayList<Integer>[] table;
	
	public SimpleClosestCityTable(int size) {
		table = new ArrayList[size];
	}
	
	// we remove 1 from the key inputed because each city corresponds to index
	// before it (e.g. city name 1 will be saved in index 0).
	public ArrayList<Integer> get(int key) {
		return table[key-1];
	}
	
	public void put(int key, ArrayList<Integer> value) {
		table[key-1] = value;
	}
}
