
// This class is the parent class for the 2-opt and 3-opt algorithms implemented.
abstract class OptPath {
	final String arrow = "->";
	private SimplePathTable path;
	public double cost;
	private double[][] distanceMatrix;
	public Point[] cities;
	
	public OptPath(Point[] cities, double[][] distanceMatrix) {
		this.cities = cities;
		this.distanceMatrix = distanceMatrix;
		path = new SimplePathTable(cities.length);
		cost = 0;
		// We initialise the path by connecting each city c to another city c+1
		// and we calculate it's cost
		for(int cityIndex = 0; cityIndex < cities.length - 1; cityIndex++) {
			path.put(cities[cityIndex].getName(), cities[cityIndex + 1].getName());
			cost+= getDistance(cities[cityIndex].getName(), cities[cityIndex+1].getName());
		}
		path.put(cities[cities.length - 1].getName(), 1);
		cost += getDistance(cities[cities.length - 1].getName(), 1);
	}
	
	// This method starts the search for optimisation until no further optimisation is possible.
	public double startSearch() {
		boolean notFinished = true;
		while(notFinished) {
			// We get possible edge swaps.
			notFinished = possibleEdgeSwaps();
		}
		return cost;
	}
	
	// These 2 abstract methods have to be implemented by any k-opt algorithm
	// that inherits from this class.
	abstract boolean possibleEdgeSwaps();
	abstract PathSwaps checkEdges(int[] edges);
	
	// Gets distance from distance matrix of 2 cities.
	public double getDistance (int city1, int city2) {
		return distanceMatrix[city1 - 1][city2 - 1];
	}
	
	// This gets the distance of a city to the city it is connected to it in the path.
	public double getCityEdge(int city) {
		return distanceMatrix[city - 1][path.get(city) - 1];
	}
	
	// This method takes a PathSwaps object and applies the changes to the path.
	public void swapEdges(PathSwaps pathSwaps) {
		for(int[] newEdge: pathSwaps.listEdges) {
			// for each element in the listEdges we change their goal city as specified.
			path.put(newEdge[0], newEdge[1]);
		}
		cost = cost - pathSwaps.oldDistance + pathSwaps.newDistance;
	}
	
	// This method takes a city and gets the city it is connected to it.
	public int getCityGoal(int city) {
		return path.get(city);
	}
	
	//This method prints the path.
	public void printPathFound() {
		System.out.println("The path is: ");	
		int nextCity = path.get(1);
		System.out.print("1" + arrow + nextCity);
		while(nextCity != 1) {
			
			nextCity = path.get(nextCity);
			System.out.print(arrow + nextCity);
		}
		System.out.print("  and the cost is: " + cost);
		System.out.println();
	}
	
	// This method creates an array representing the path.
	public int[] arrayPath() {
		int[] result = new int[cities.length+1];
		result[0] = 1;
		int nextCity = 1;
		int index = 1;
		do {
			nextCity = path.get(nextCity);
			result[index++] = nextCity;
		}
		while(nextCity != 1);
		return result;
		
	}
}
