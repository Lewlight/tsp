import java.io.*;
import java.lang.Integer;
import java.util.*;

/*To run an algorithm you start by creating an object of the algorithm class
 (HeldKarpAlgorithm, DikstraStack, FastOptTwo ...), and then calling 
 the startSearch method which runs the algorithm then returns the cost of the path found.
 To print the path found after it finishes running you will need to call the method printPathFound()
 except for HeldKarpAlgorithm which doesn't have that method, instead running the algorithm
 automatically prints the result at the end.
 To run these algorithms you will need to run loadCities(fileName, numberOfCities), which 
 returns an array of cities Point[]. All classes (except  Dijkstra) take the cities array
 Point[], and the distance matrix (double[][]) (which we can get with calculateDistanceMatrix(cities))
 as arguments.
*/ 

// This is the class containing the main method, where execution starts.
public class Runner {
	//private static final String fileName = "test1tsp.txt";
	private static final String fileName = "test4-21.txt";
	private static final int numberOfCities = 27;
	private static final String spaceSeparator = " ";
	private static final String tabSeparator = "\t";
	
	// This is the main method where execution starts with examples of 
	// running every algorithm implemented.
	public static void main(String[] args) {
		
		Point[] cities = loadCities(fileName, numberOfCities);
		double[][] distanceMatrix = calculateDistanceMatrix(cities);
		
		/*
		OptPath firstAlgorithm = new OptThree(cities, distanceMatrix);
		firstAlgorithm.startSearch();
		firstAlgorithm.printPathFound();
		
		// The third argument could be adjusted to find better or worst results.
		
		
		BestFirstSearch thirdAlgorithm = new BestFirstSearch(cities, distanceMatrix);
		thirdAlgorithm.startSearch();
		thirdAlgorithm.printPathFound();
		
		DijkstraStack fifthAlgorithm = new DijkstraStack(cities);
		fifthAlgorithm.startSearch();
		fifthAlgorithm.printPathFound();
		
		OptPath sixthAlgorithm = new OptTwo(cities, distanceMatrix);
		sixthAlgorithm.startSearch();
		sixthAlgorithm.printPathFound();
		
		HeldKarpAlgorithm fourthAlgorithm = new HeldKarpAlgorithm(cities, distanceMatrix);
		fourthAlgorithm.startSearch();
		*/
		OptPath sixthAlgorithm = new OptTwo(cities, distanceMatrix);
		sixthAlgorithm.startSearch();
		sixthAlgorithm.printPathFound();
		
		long time1 = System.nanoTime();
		BestFirstSearch secondAlgorithm = new BestFirstSearch(cities, distanceMatrix);
		
		double cost = secondAlgorithm.startSearch();
		time1 = System.nanoTime() - time1;
		secondAlgorithm.printPathFound();
		System.out.println("The time is: " + (double)time1/1000000 + " and the result " + time1*cost);
	}
	
	// This method takes a file path and the number of cities in that file and it creates an array of cities from it.
	public static Point[] loadCities(String filePath, int numberCities) {
		Point[] cityArray = new Point[numberCities];
		int index = 0;
		// we use a BufferedReader to read the file in line by line
		try (FileReader reader = new FileReader(filePath);
		     BufferedReader bufferedReader = new BufferedReader((reader))) {
			String line;
			while ((line = bufferedReader.readLine()) != null) {
				// If a line is "empty" or has only the separator skip to the next.
				if(line.isEmpty() || line.equals(tabSeparator) || line.equals(spaceSeparator)) {
					continue;
			  	}
				// we split the line using "\t" to get the separate parameters
			    String[] params = line.split(tabSeparator);
			    // if the separating character is not a tab we try space.
			    if(params.length == 1) {
			    	params = line.split(spaceSeparator);
			    }
			    // This check deals with separators with more than just one tab or space.
			    if(params.length > 3){
			    	String[] temporarySplit = new String[3];
			    	int paramIndex = 0;
			    	// we keep only the String that are not empty or are separators.
			    	for(String parameter: params){
			    		if(!parameter.isEmpty() && parameter != tabSeparator && parameter != spaceSeparator){
			    			temporarySplit[paramIndex++] = parameter;
			    		}
			    	}
			    	params = temporarySplit;
			    }
			    if(params.length != 3) {
			    	System.out.println("Some problem occured when seperating the file.");
			    	System.out.println("The line that has the problem is: " + line);
			    	System.exit(1);
			    }
			    // The first part from the split line is the city name.
				int cityName = Integer.parseInt(params[0]);
				// The second part from the split line is the x axis position.
				double xPosition = Double.parseDouble(params[1]);
				// The third part from the split line is the y axis position.
				double yPosition = Double.parseDouble(params[2]);
				// We create the Point object representing a city.
				Point city = new Point(xPosition, yPosition, cityName);
				// We put the city in the array.
				cityArray[index++] = city;
			  }
		}
		catch(IOException ex){
			System.out.println("IOException encountered: " + ex.getMessage());
			System.exit(1);
		}
		return cityArray;
	}
	
	// This method was created for testing; it generates a number of random points (cities).
	public static Point[] randCities(int num, int coordinateRange) {
		ArrayList<Point> cityList = new ArrayList<Point>();
		for(int cityIndex = 0; cityIndex < num; cityIndex++) {
			int xPosition = (int)(Math.random()*coordinateRange);
			// we randomise the negative numbers as well.
			if(Math.random()> 0.5) {
				xPosition = -1*xPosition;
			}
			int yPosition = (int)(Math.random()*coordinateRange);
			if(Math.random()> 0.5) {
				yPosition = -1*yPosition;
			}
			Point city = new Point(xPosition, yPosition, cityIndex+1);
			cityList.add(city);
		}
		return cityList.toArray(new Point[0]);
	}
	
	public static double[][] calculateDistanceMatrix(Point[] cities){
		// This method calculates the distance matrix from the points.
		double[][] distances = new double[cities.length][cities.length];
		for(Point city: cities) {
			for(Point otherCity: cities) {
				distances[city.getName() - 1][otherCity.getName() - 1] = city.distanceToPoint(otherCity);
			}
		}
		return distances;
	}
}
