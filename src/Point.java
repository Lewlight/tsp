import java.lang.Math;

// This class represents a Point on a 2D plane used to represent a city.
public class Point {
	private double x, y;
	private int name;
	
	public Point(double x, double y, int name) {
		this.x = x;
		this.y = y;
		this.name = name;
	}
	
	public double getX() {
		return x;
	}
	
	public int getName() {
		return name;
	}
	
	public double getY() {
		return y;
	}
	
	// This method gets the distance from this Point to another Point
	public double distanceToPoint(Point otherPoint) {
		return Math.sqrt(Math.pow(x - otherPoint.getX(), 2) + Math.pow(y - otherPoint.getY(), 2));
	}
	
	public String toString() {
		return "" + name;
	}
}
