// This is a Node class that stores a key and a value and a node connected to it.
public class Node<V> {
	public int[] key;
	public V value;
	public Node<V> next;
	
	public Node(int[] key, V value, Node<V> next) {
		this.key = key;
		this.value = value;
		this.next = next;
	}
	
	public Node(int[] key, V value) {
		this(key, value, null);
	}
	
	public Node(Node<V> node) {
		this(node.key, node.value, node.next);
	}
}
